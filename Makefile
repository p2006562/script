CFLAGS = -Wall -ggdb

all: bin/projet

bin/projet: obj/mainTest.o obj/Image.o
	g++ $(CFLAGS) obj/mainTest.o obj/Image.o -o projet 

obj/mainTest.o: src/mainTest.cpp src/Image.h src/Pixel.h
	g++ $(CFLAGS) -c src/mainTest.cpp -o obj/mainTest.o

obj/Image.o: src/Image.cpp src/Image.h src/Pixel.h
	g++ $(CFLAGS) -c src/Image.cpp -o obj/Image.o


clean:	
	rm obj/*.o 

