#ifndef _PIXEL_H
#define _PIXEL_H
#include <iostream>

	struct Pixel {
		unsigned char r,g,b;

		// Constructeur par défaut de la classe: initialise le pixel à la couleur noire
		Pixel() {
			r = 0 ;
			g = 0 ;
			b = 0 ;
		};

		// Constructeur de la classe: initialise r,g,b avec les paramètres
		Pixel (int nr, int ng, int nb){
			r = nr ;
			g = ng ;
			b = nb ;
 		};
	};
		
	 
#endif
