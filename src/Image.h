#ifndef _IMAGE_H
#define _IMAGE_H
#include "Pixel.h"

class Image {
	private:
		Pixel * tab ;
		int dimx,dimy ;

	public: 
		// Constructeur de la classe : initialise dimx et dimy à 0
 		// n’alloue aucune mémoire pour le tableau de pixel		
		Image();
		// Constructeur de la classe : initialise dimx et dimy (après vérification)
 		// puis alloue le tableau de pixel dans le tas (image noire)
 		Image(int dimensionX, int dimensionY);
 		// Destructeur de la classe : déallocation de la mémoire du tableau de pixels
 		// et mise à jour des champs dimx et dimy à 0
		~Image();
		// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
		Pixel getPix(const int x, const int y);
		// Modifie le pixel de coordonnées (x,y)
		void setPix(const int x , const int y, const Pixel couleur);

		static void testRegression();

		void dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax, Pixel couleur);

};
#endif