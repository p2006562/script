#include "Image.h"
#include <assert.h>
#include <iostream>
using namespace std ;

Image::Image(){
			dimx = 0 ;
			dimy = 0 ;
}

Image::Image(const int dimensionX, const int dimensionY){
	if ((dimensionX > 0)&&(dimensionY > 0)){
		dimx = dimensionX ;
		dimy = dimensionY ;
		Pixel * tab = new Pixel[dimx * dimy] ;
		for(int i = 0 ; i<dimx*dimy; i++){
			tab[i].r = 0 ;
			tab[i].g = 0 ;
			tab[i].b = 0 ;
		}
	}
}

Image::~Image(){
	delete [] tab ;
	dimx = 0 ;
	dimy = 0 ;
}

Pixel Image::getPix(const int x, const int y){
	if ((x > 0)&&(x <= dimx)&&((y > 0)&&(x <= dimy))){
		return tab[y*dimx+x] ;
	}
	else{
		Pixel noir ;
		return noir ;
	}
} 


void Image::setPix(const int x, const int y, const Pixel couleur){
	if ((x > 0)&&(x <= dimx)&&((y > 0)&&(x <= dimy))){
		tab[y*dimx+x].r = couleur.r ;
		tab[y*dimx+x].g = couleur.g ;
		tab[y*dimx+x].b = couleur.b ;
	}
}

void Image::testRegression(){
	// On teste l'image crée avec le constructeur par défaut
	Image im1 ; 
	assert(im1.dimx == 0 ) ;
	assert(im1.dimy == 0 ) ;

	// On test l'image crée avec de constructeur avec paramètres 
	Image im2(2,3) ;
	assert(im2.dimx == 2 ) ;
	assert(im2.dimy == 3 ) ;	

	// On teste le changement de valeur d'un pixel avec la procédure setPix
	Pixel p1(123,9,45);
	im2.setPix(1,2,p1);
	assert(im2.tab[2*im2.dimx+1].r == 123);
	assert(im2.tab[2*im2.dimx+1].g == 9);
	assert(im2.tab[2*im2.dimx+1].b == 45);

	// On teste la méthode getPix 
	Pixel verif = im2.getPix(1,2);
	assert(verif.r == p1.r);
	assert(verif.g == p1.g);
	assert(verif.b == p1.b);

	// On vérifie que le rectangle créé est bien réalisé
	int xmin = 3 ;
	int ymin = 3 ;
	int xmax = 6 ;
	int ymax = 7 ;

	Image im3(10,10);
	im3.dessinerRectangle(xmin,ymin,xmax,ymax,p1);
/*
	for(int i = xmin ; i <= xmax ; i++){
		// ligne du bas
		assert(im3.tab[ymax*im3.dimx+i].r == p1.r) ;
		assert(im3.tab[ymax*im3.dimx+i].g == p1.g) ;
		assert(im3.tab[ymax*im3.dimx+i].b == p1.b) ;

		// Ligne du haut
		assert(im3.tab[ymin*im3.dimx+i].r == p1.r) ;
		assert(im3.tab[ymin*im3.dimx+i].g == p1.g) ;
		assert(im3.tab[ymin*im3.dimx+i].b == p1.b) ;
	}
	for(int y = ymin ; y <= ymax  ; y++){
		// Ligne de droite
		assert(im3.tab[y*im3.dimx+xmax].r == p1.r) ;
		assert(im3.tab[y*im3.dimx+xmax].g == p1.g) ;
		assert(im3.tab[y*im3.dimx+xmax].b == p1.b) ;

		// Ligne de gauche
		assert(im3.tab[y*im3.dimx+xmin].r == p1.r) ;
		assert(im3.tab[y*im3.dimx+xmin].g == p1.g) ;
		assert(im3.tab[y*im3.dimx+xmin].b == p1.b) ;
	}
	*/
};

void Image::dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax, Pixel couleur){
	if (((Xmin >= 0)&&(Ymin >= 0)) && ((Xmax <= dimx)&&(Ymax <= dimy))){ 
		// On trace les 2 segments verticaux
		for(int y = Ymin ; y <= Ymax ; y++){
			cout <<"\n" << int(couleur.r) ;
			tab[y*dimx+Xmin].r = couleur.r ;
			tab[y*dimx+Xmin].g = couleur.g ;
			tab[y*dimx+Xmin].b = couleur.b ;
					
			tab[y*dimx+Xmax].r = couleur.r;
			tab[y*dimx+Xmax].g = couleur.g;
			tab[y*dimx+Xmax].b = couleur.b;
		}
		// On trace les 2 segments horizontaux
		for(int i = Xmin ; i <= Xmax ; i++){
			cout << "\n" << Ymin*dimx+i ;
			tab[Ymin*dimx+i].r = couleur.r ;
			tab[Ymin*dimx+i].g = couleur.g ;
			tab[Ymin*dimx+i].b = couleur.b ;
					
			tab[Ymax*dimx+i].r = couleur.r;
			tab[Ymax*dimx+i].g = couleur.g;
			tab[Ymax*dimx+i].b = couleur.b;
			} 
		}
	}

 


